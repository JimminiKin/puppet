Exec {
    path => '/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin:/usr/local/sbin/',
    timeout => 60,
    tries   => 3
}

# Stages
stage { 'start': }
stage { 'setup_env': }
stage { 'setup_sources': }
Stage['start'] -> Stage['setup_env'] -> Stage['setup_sources'] -> Stage['main']

# Start class to declared user/group
class owner {
    # www is mount point on DEV
    if $::host_uid {
        $admin = $::host_uid
        $wwwdata = $::host_uid
    } else {
        $admin = 'admin'
        $wwwdata = 'www-data'
    }
}

class group {
    # www is mount point on DEV
    if $::host_gid {
        $admin = $::host_gid
        $wwwdata = $::host_gid
    } else {
        $admin = 'admin'
        $wwwdata = 'www-data'
    }
}

notice "------------------------------------"
notice "Installation Puppet"
notice ">> FQDN : $::fqdn"
notice ">> CLUSTER : $::cluster"
notice ">> ROLES : $::roles"
notice ">> ENV : $::env"
notice "------------------------------------"

# Baseconfig
class { 'owner': stage => 'start' }
class { 'group': stage => 'start' }
class { 'wedzem::baseconfig::env': stage => 'setup_env' }
class { 'wedzem::baseconfig::sources': stage => 'setup_sources' }

# Hiera common
hiera_include('common_classes')

# Hiera by role
each(split($::roles, ',')) |$role| {
    hiera_include("${role}_classes", '')
}
