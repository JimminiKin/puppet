class wedzem::packages::mod_php53 {

    # Installation PHP
    include php
    include php::fpm
    include php::cli
    include php::pear
    include php::extension::curl
    include php::extension::intl
    include php::extension::gd
    include php::extension::memcache
    include php::extension::mcrypt
    include php::extension::imap
    include php::extension::imagick
    class { "php::extension::mysql": package => 'php5-mysql' }
    package { "php5-dev": require => Class['php::fpm']; }
    package { "php5-xsl": require => Class['php::fpm']; }  # Needed for supervision's widget bugtracker/mestickets

    # Extenion Mongo for Drupal
    class { "php::extension::mongo":
            require => [
                Class['php'],
                Class['php::pear'],
                Class['php::fpm'],
                Class['php::cli'],
                Package['php5-dev']
            ];
    }

    # Active extensions
    file {
        '/etc/php5/conf.d/mongo.ini':
            ensure => present,
            content => 'extension=mongo.so',
            require => [
                    Class['php'],
                    Class['php::pear'],
                    Class['php::fpm'],
                    Class['php::extension::mongo']
                ],
            notify  => Service['php5-fpm'];

        '/etc/php5/conf.d/mysql.ini':
            ensure => present,
            content => 'extension=mysql.so',
            require => [
                    Class['php'],
                    Class['php::pear'],
                    Class['php::fpm'],
                    Class['php::extension::mysql']
                ],
            notify  => Service['php5-fpm'];

        '/etc/php5/conf.d/mysqli.ini':
            ensure => present,
            content => 'extension=mysqli.so',
            require => [
                    Class['php'],
                    Class['php::pear'],
                    Class['php::fpm'],
                    Class['php::extension::mysql']
                ],
            notify  => Service['php5-fpm'];

        '/etc/php5/conf.d/curl.ini':
            ensure => present,
            content => 'extension=curl.so',
            require => [
                    Class['php'],
                    Class['php::pear'],
                    Class['php::fpm'],
                    Class['php::extension::curl']
                ],
            notify  => Service['php5-fpm'];

        '/etc/php5/conf.d/gd.ini':
            ensure => present,
            content => 'extension=gd.so',
            require => [
                    Class['php'],
                    Class['php::pear'],
                    Class['php::fpm'],
                    Class['php::extension::gd']
                ],
            notify  => Service['php5-fpm'];

        '/etc/php5/conf.d/memcache.ini':
            ensure => present,
            content => 'extension=memcache.so',
            require => [
                    Class['php'],
                    Class['php::pear'],
                    Class['php::fpm'],
                    Class['php::extension::memcache']
                ],
            notify  => Service['php5-fpm'];

        '/etc/php5/conf.d/mcrypt.ini':
            ensure => present,
            content => 'extension=mcrypt.so',
            require => [
                    Class['php'],
                    Class['php::pear'],
                    Class['php::fpm'],
                    Class['php::extension::mcrypt']
                ],
            notify  => Service['php5-fpm'];

        '/etc/php5/conf.d/xsl.ini':
            ensure => present,
            content => 'extension=xsl.so',
            require => [
                    Class['php'],
                    Class['php::pear'],
                    Class['php::fpm'],
                    Package['php5-xsl']
                ],
            notify  => Service['php5-fpm'];

        '/etc/php5/conf.d/imap.ini':
            ensure => present,
            content => 'extension=imap.so',
            require => [
                    Class['php'],
                    Class['php::pear'],
                    Class['php::fpm']
                ],
            notify  => Service['php5-fpm'];
    }

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Config POOL FPM

    php::fpm::pool { 'www' :
        listen => '/var/run/php5-fpm.sock',
        listen_owner => 'www-data',
        listen_group => 'www-data',
        listen_mode => '660',
        user => 'www-data',
        group => 'www-data',
        pm => 'dynamic',
        pm_max_children => '60',
        pm_start_servers => '4',
        pm_min_spare_servers => '4',
        pm_max_spare_servers => '8',
        pm_max_requests => '500',
        pm_process_idle_timeout => '10s',
        ping_response => 'pong',
        request_terminate_timeout => '0',
        request_slowlog_timeout => '3s',
        pm_status_path => '/fpm/status.php',
        slowlog => "/var/log/php5-fpm-www-slow.log"
    }

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Config common (http://gregrickaby.com/the-perfect-apc-configuration/)

    $config_common = [
        "set PHP/short_open_tag On",
        "set PHP/log_errors On",
        "set PHP/upload_max_filesize 30M",
        "set PHP/post_max_size 30M",
        "set Date/date.timezone Europe/Paris"
    ]

    php::fpm::config{ 'common_phpfpm': config => $config_common, require => Class['php::fpm']; }
    php::cli::config{ 'common_phpcli': config => $config_common, require => Class['php::cli']; }

    # Custom case
    php::fpm::config{ 'maxexec_phpfpm': config => ["set PHP/max_execution_time 120"], require => Class['php::fpm']; }
    php::cli::config{ 'maxexec_phpcli': config => ["set PHP/max_execution_time 0"], require => Class['php::cli']; }
    php::fpm::config{ 'memory_limit_phpfpm': config => ["set PHP/memory_limit 768M"], require => Class['php::fpm']; }
    php::cli::config{ 'memory_limit_phpcli': config => ["set PHP/memory_limit 768M"], require => Class['php::cli']; }

    # Config to show all errors (only on DEV and PHP_CLI)
    $config_all = [
        "set PHP/expose_php On",
        "set PHP/track_errors On",
        "set PHP/display_errors On",
        "set PHP/error_reporting E_ALL"
    ]

    # Config for production
    $config_restricted = [
        "set PHP/expose_php Off",
        "set PHP/track_errors Off",
        "set PHP/display_errors Off",
        "set PHP/error_reporting E_ALL & ~E_DEPRECATED & ~E_STRICT"
    ]

    case $::env {
        'prod': {
            php::fpm::config{ "config_${::env}_phpfpm": config => $config_restricted, require => Class['php::fpm']; }
            php::cli::config{ "config_${::env}_phpcli": config => $config_all, require => Class['php::cli']; }
        }
        'preprod': {
            php::fpm::config{ "config_${::env}_phpfpm": config => $config_restricted, require => Class['php::fpm']; }
            php::cli::config{ "config_${::env}_phpcli": config => $config_all, require => Class['php::cli']; }
        }
        'integ': {
            php::fpm::config{ "config_${::env}_phpfpm": config => $config_all, require => Class['php::fpm']; }
            php::cli::config{ "config_${::env}_phpcli": config => $config_all, require => Class['php::cli']; }
        }
        'dev': {
            php::fpm::config{ "config_${::env}_phpfpm": config => $config_all, require => Class['php::fpm']; }
            php::cli::config{ "config_${::env}_phpcli": config => $config_all, require => Class['php::cli']; }
        }
    }

    # Port Apache for Munin
    if "cache" in $::roles {
        $port = '8080'
    } else {
        $port = '80'
    }

    # Mise en place Munin
    Class['php'] -> munin::plugin { 'phpfpm_average': linkplugins => true, source => 'wedzem/packages/munin/php/phpfpm_average' }
    Class['php'] -> munin::plugin { 'phpfpm_connections': linkplugins => true, source => 'wedzem/packages/munin/php/phpfpm_connections' }
    Class['php'] -> munin::plugin { 'phpfpm_memory': linkplugins => true, source => 'wedzem/packages/munin/php/phpfpm_memory' }
    Class['php'] -> munin::plugin { 'phpfpm_processes': linkplugins => true, source => 'wedzem/packages/munin/php/phpfpm_processes' }
    Class['php'] -> munin::plugin { 'phpfpm_status': linkplugins => true, source => 'wedzem/packages/munin/php/phpfpm_status' }
    Class['php'] -> munin::plugin { 'php': content_config => template('wedzem/munin/php.erb') }
}