class wedzem::packages::mod_resolver {

    $DNSProdMaster = "8.8.8.8";
    $DNSProdSlave = "8.8.4.4";
    $DNSIntegSlave = "8.8.4.4";
    $DNSMasterInsternet = "8.8.8.8";
    $DNSSLaveInternet = "8.8.4.4";

    case $::env {
        "dev" : { $server = [ $DNSIntegSlave ] }
        "integ" : { $server = [ $DNSIntegSlave , $DNSProdMaster ] }
        "prod" : { $server = [ $DNSProdMaster , $DNSProdSlave ] }
        "preprod" : { $server = [ $DNSProdMaster , $DNSProdSlave ] }
        default : { $server = [ $DNSMasterInsternet , $DNSSLaveInternet ] }
    }

    class { 'resolver':
        dns_servers => $server,
        search      => [ 'wedzem.com'],
    }

}