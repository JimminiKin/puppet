class wedzem::packages::mod_jenkins {

    class { 'jenkins':
        version => 'latest',
        repo    => false
    }

    rbenv::install { "jenkins":
        group => 'jenkins',
        home  => '/var/lib/jenkins',
    }

    rbenv::compile { "2.1.0":
        user => "jenkins",
        home => "/var/lib/jenkins",
    }

    # Mise en place des répertoires 'www', '.ssh' et 'logs'
    file {
        '/var/lib/jenkins/.bashrc':
            owner => 'jenkins',
            group => 'jenkins',
            mode  => '0640',
            source => 'puppet:///modules/wedzem/users/bashrc_jenkins';

        '/var/lib/jenkins/.gitconfig' :
            owner => 'jenkins',
            group => 'jenkins',
            mode  => '0640',
            source => 'puppet:///modules/wedzem/packages/jenkins/gitconfig',
            require => Class['jenkins'];

        '/var/lib/jenkins/.bash_aliases':
            owner => 'jenkins',
            group => 'jenkins',
            mode  => '0640',
            source => 'puppet:///modules/wedzem/users/bash_aliases';

        '/var/lib/jenkins/.profile':
            owner => 'jenkins',
            group => 'jenkins',
            mode  => '0640',
            source => 'puppet:///modules/wedzem/users/profile';
    }

    jenkins::plugin { "ansicolor" : ; }
    jenkins::plugin { "role-strategy" : ; }
    jenkins::plugin { "gravatar" : ; }
    jenkins::plugin { "thinBackup" : ; }
    jenkins::plugin { "timestamper" : ; }
    jenkins::plugin { "simple-theme-plugin" : ; }
    jenkins::plugin { "git-client" : ; }
    jenkins::plugin { "scm-api" : ; }
    jenkins::plugin { "git" : ; }
    jenkins::plugin { "groovy-postbuild" : ; }
    jenkins::plugin { "parameterized-trigger" : ; }
    jenkins::plugin { "extended-choice-parameter" : ; }
    jenkins::plugin { "build-blocker-plugin" : ; }

    # Installation Apache
    apache::vhost {
        'vhost_jenkins':
            add_listen      => true,
            setenv          => ["RC_ENV ${::env}", "proxy-nokeepalive 1"],
            ip              => '*',
            port            => '80',
            docroot         => '/var/lib/jenkins/',
            docroot_owner   => 'jenkins',
            docroot_group   => 'nogroup',
            servername      => 'forge.wedzem.dev',
            serveraliases   => "forge.rc-${::env}.com",
            custom_fragment => '
                ProxyPreserveHost On
                ProxyRequests     Off
                ProxyPass         /  http://localhost:8080/ nocanon
                ProxyPassReverse  /  http://localhost:8080/
                AllowEncodedSlashes NoDecode

                <Proxy http://localhost:8080/*>
                    Order deny,allow
                    Allow from all
                </Proxy>',
            require         => Class['jenkins']
    }

}
