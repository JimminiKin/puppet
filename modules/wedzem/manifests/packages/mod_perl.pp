class wedzem::packages::mod_perl {

    include perl

    perl::module { 'Bundle::LWP': }

    package{'libjson-perl':}

    perl::module { 'JSON::PP': 
        require => Package['libjson-perl']
    }

}
