class wedzem::packages::mod_afp {

    if $::env == 'dev' {
        package { 'netatalk':
            ensure => installed,
        }
    }
}