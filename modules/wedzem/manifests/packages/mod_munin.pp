class wedzem::packages::mod_munin {

    if $::cluster == 'monitor' {
        $server_local = true
    } else {
        $server_local = false
    }

    # Mise en place Munin NODES
    case $::env {
        'dev': { $muninserver = [
            '192.168.136.228',
            '10.6.136.228' # http://monitor.wedzem.rc-integ.com
            ]
        }
        'integ': { $muninserver = [
            '10.6.136.228' # http://monitor.wedzem.rc-integ.com
            ]
        }
        default: { $muninserver = [
            '10.12.0.6', # http://monitor.wedzem.fr
            '10.12.0.1'  # http://munin.dnsroute.fr/munin
            ]
        }
    }

    class { 'munin':
        server_local => $server_local,
        server => $muninserver,
        address => $::ipaddress,
        autoconfigure => false,
        graph_strategy => 'cgi'
    }

}