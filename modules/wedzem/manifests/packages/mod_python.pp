class wedzem::packages::mod_python {

    class { 'python':
      version    => 'system',
      pip        => true,
      dev        => false,
      virtualenv => false,
      gunicorn   => false,
    }

}