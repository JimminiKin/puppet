class wedzem::packages::mod_powerdns {
		
		notify {"Installation de PowerDNS":}
		include powerdns


 		notify {"Creation du repertoire /etc/powerdns/":}
		file { "/etc/powerdns/":
    		ensure => "directory",
    		owner  => "admin",
    		group  => "admin",
    		mode   => 775,
		}

}

