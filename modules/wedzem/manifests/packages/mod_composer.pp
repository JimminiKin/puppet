class wedzem::packages::mod_composer {

        # Installation Composer
        class { 'composer':
            command_name => 'composer',
            target_dir   => '/usr/local/bin',
            auto_update  => true,
            require      => Class['Php::Cli'];
        }

        file {
            '/home/admin/.composer':
                ensure  => 'directory',
                owner   => 'admin',
                group   => 'www-data',
                mode    => '0755',
                require => Class['composer'];

            '/home/admin/.composer/config.json':
                owner   => 'admin',
                group   => 'www-data',
                mode    => '0644',
                source => "puppet:///modules/wedzem/packages/composer/config.json",
                require => File['/home/admin/.composer'];
        }

}
