class wedzem::packages::mod_nrpe {

    # Mise en place du Serveur NRPE pour le monitorring de Nagios
    class { 'nrpe':
        allowed_hosts => ['127.0.0.1','178.33.169.149','10.12.0.5'],
        template => 'wedzem/nrpe.cfg.erb'
    }

    nrpe::plugin { 'check_nfs': source => 'wedzem/packages/nrpe/check_nfs' }
    nrpe::plugin { 'check_backend.sh': source => 'wedzem/packages/nrpe/check_backend.sh' }
}