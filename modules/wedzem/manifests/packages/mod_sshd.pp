class wedzem::packages::mod_sshd {

  if $::env == 'dev' {
      $ssh_rules = 'yes'
  } else {
      $ssh_rules = 'no'
  }


  if $::fqdn != 'gitlab.fr.wedzem.com' {
    # Config SSH
    augeas { 'sshd_config':
      context => "/files/etc/ssh/sshd_config",
      changes => [
        "set PermitRootLogin $ssh_rules",
        "set PermitEmptyPasswords $ssh_rules",
        "set RSAAuthentication yes",
        "set PubkeyAuthentication yes",
        "set PasswordAuthentication $ssh_rules",
      ],
      notify => Service[sshd]
    }

    service { 'sshd':
      name  => 'ssh',
      pattern => 'sshd',
      ensure => 'running',
      enable => true
    }
  }

}
