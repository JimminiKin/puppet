class wedzem::packages::mod_mysql {

    # Installation MySQL
    class { 'mysql':
        root_password => 'kVXFkgXhFyHaBKpUX5w4BLFNmJhr',
    }

    # Root GRANT access
    mysql::grant {
        "root_access_localhost" :
            mysql_privileges     => 'ALL',
            mysql_db             => '*',
            mysql_user           => 'root',
            mysql_password       => 'kVXFkgXhFyHaBKpUX5w4BLFNmJhr',
            mysql_host           => 'localhost',
            mysql_create_db      => false
    }

    mysql::grant {
        "root_access_all" :
            mysql_privileges     => 'ALL',
            mysql_db             => '*',
            mysql_user           => 'root',
            mysql_password       => 'kVXFkgXhFyHaBKpUX5w4BLFNmJhr',
            mysql_host           => '%',
            mysql_create_db      => false
    }

    # Performance tuning
    File['mysql.conf'] -> mysql::augeas {
        'mysqld/key_buffer': value  => '16M';
        'mysqld/key_buffer_size': value  => '128M';
        'mysqld/table_open_cache': value  => '4096';
    }

    case $::env {
        'dev': {
            File['mysql.conf'] -> mysql::augeas {
                'mysqld/query_cache_size':value  => '16M';
                'mysqld/innodb_buffer_pool_size':value  => '16M';
                'mysqld/query_cache_type': value  => '1';
                'mysqld/query_cache_limit': value  => '4M';
                'mysqld/max_connections': value  => '250';
            }
        }
        'integ':{
            File['mysql.conf'] -> mysql::augeas {
                'mysqld/query_cache_size':value  => '32M';
                'mysqld/innodb_buffer_pool_size':value  => '32M';
                'mysqld/query_cache_type': value  => '1';
                'mysqld/query_cache_limit': value  => '4M';
                'mysqld/max_connections': value  => '250';
            }
        }
        'preprod':{
            File['mysql.conf'] -> mysql::augeas {
                'mysqld/query_cache_size':value  => '64M';
                'mysqld/innodb_buffer_pool_size':value  => '128M';
                'mysqld/query_cache_type': value  => '1';
                'mysqld/query_cache_limit': value  => '4M';
                'mysqld/max_connections': value  => '250';
            }
        }        
        default: {
            case $::cluster {
                'drupal6autre': {
                    File['mysql.conf'] -> mysql::augeas {
                        'mysqld/innodb_buffer_pool_size':value  => '4G';
                        'mysqld/query_cache_size':value  => '1024M';
                        'mysqld/tmp_table_size':value  => '256M';
                        'mysqld/max_connections':value => '400';
                        'mysqld/table_cache':value => '5000';
                        'mysqld/query_cache_limit': value  => '8M';
                    }
                }
                default:{
                    File['mysql.conf'] -> mysql::augeas {
                        'mysqld/query_cache_size':value  => '256M';
                        'mysqld/innodb_buffer_pool_size':value  => '1G';
                        'mysqld/query_cache_type': value  => '1';
                        'mysqld/query_cache_limit': value  => '4M';
                        'mysqld/max_connections': value  => '250';
                    }
                }  
            }

        }

    }


    # Root GRANT access for PHPmyAdmin
    case $::env {
        'dev': {
            $ip_phpmyadmin = '127.0.0.1'
        }
        default: {
            $ip_phpmyadmin = '10.6.136.220'
        }
    }

    mysql::grant {
        "root_access_phpmyadmin" :
            mysql_privileges     => 'ALL',
            mysql_db             => '*',
            mysql_user           => 'root',
            mysql_password       => 'kVXFkgXhFyHaBKpUX5w4BLFNmJhr',
            mysql_host           => $ip_phpmyadmin, #phpmyadmin.wedzem.dev
            mysql_create_db      => false
    }

    # Mise en place Munin
    Class['mysql'] -> munin::plugin { 'mysql_': linkplugins => true }
    Class['mysql'] -> munin::plugin { 'mysql_bytes': linkplugins => true }
    Class['mysql'] -> munin::plugin { 'mysql_queries': linkplugins => true }
    Class['mysql'] -> munin::plugin { 'mysql_slowqueries': linkplugins => true }
    Class['mysql'] -> munin::plugin { 'mysql_threads': linkplugins => true }

}