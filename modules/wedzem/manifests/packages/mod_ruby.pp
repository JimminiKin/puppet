class wedzem::packages::mod_ruby {

    rbenv::install { "admin":
        group => 'admin',
        home  => '/home/admin',
    }

    rbenv::compile { "2.1.2":
        user => "admin",
        global => true,
        home  => '/home/admin',
    }

}