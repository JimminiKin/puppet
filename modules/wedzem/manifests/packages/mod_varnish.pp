class wedzem::packages::mod_varnish {

    # Mise en place Munin Standard
    Class['varnish'] -> munin::plugin { 'varnish_': linkplugins => true, source => 'wedzem/packages/munin/varnish/varnish_' }
    Class['varnish'] -> munin::plugin { 'varnish_request_rate': linkplugins => true, source => 'wedzem/packages/munin/varnish/varnish_' }
    Class['varnish'] -> munin::plugin { 'varnish_hit_rate': linkplugins => true, source => 'wedzem/packages/munin/varnish/varnish_' }
    Class['varnish'] -> munin::plugin { 'varnish_backend_traffic': linkplugins => true, source => 'wedzem/packages/munin/varnish/varnish_' }
    Class['varnish'] -> munin::plugin { 'varnish_objects': linkplugins => true, source => 'wedzem/packages/munin/varnish/varnish_' }
    Class['varnish'] -> munin::plugin { 'varnish_transfer_rates': linkplugins => true, source => 'wedzem/packages/munin/varnish/varnish_' }
    Class['varnish'] -> munin::plugin { 'varnish_threads': linkplugins => true, source => 'wedzem/packages/munin/varnish/varnish_' }
    Class['varnish'] -> munin::plugin { 'varnish_memory_usage': linkplugins => true, source => 'wedzem/packages/munin/varnish/varnish_' }
    Class['varnish'] -> munin::plugin { 'varnish_uptime': linkplugins => true, source => 'wedzem/packages/munin/varnish/varnish_' }
    Class['varnish'] -> munin::plugin { 'varnish_allocations': linkplugins => true, source => 'wedzem/packages/munin/varnish/varnish_' }
    Class['varnish'] -> munin::plugin { 'varnish_expunge': linkplugins => true, source => 'wedzem/packages/munin/varnish/varnish_' }
    Class['varnish'] -> munin::plugin { 'varnish_bad': linkplugins => true, source => 'wedzem/packages/munin/varnish/varnish_' }
    Class['varnish'] -> munin::plugin { 'varnish_gzip': linkplugins => true, source => 'wedzem/packages/munin/varnish/varnish_' }
    Class['varnish'] -> munin::plugin { 'varnish_session': linkplugins => true, source => 'wedzem/packages/munin/varnish/varnish_' }
    Class['varnish'] -> munin::plugin { 'varnish': content_config => template('wedzem/munin/varnish.erb') }

    # Mise en place Munin Custom
    Class['varnish'] -> munin::plugin { 'varnishncsa_errors500': linkplugins => true, source => 'wedzem/packages/munin/logger/errors' }
    Class['varnish'] -> munin::plugin { 'varnishncsa_errors503': linkplugins => true, source => 'wedzem/packages/munin/logger/errors' }
    Class['varnish'] -> munin::plugin { 'varnishncsa_errors403': linkplugins => true, source => 'wedzem/packages/munin/logger/errors' }
    Class['varnish'] -> munin::plugin { 'varnishncsa_ban': linkplugins => true, source => 'wedzem/packages/munin/logger/errors' }
    Class['varnish'] -> munin::plugin { 'varnishncsa': content_config => template('wedzem/munin/varnishncsa.erb') }

    # Enabled logs Varnish
    file {
        '/etc/default/varnishncsa':
            owner => 'root',
            group => 'root',
            mode  => '0644',
            source => 'puppet:///modules/wedzem/packages/varnish/varnishncsa',
            require => Class['varnish'],
            notify => Service['varnishncsa']
    }

    service { 'varnishncsa':
        ensure => 'running',
        name   => 'varnishncsa',
        enable => true,
        require => Class['varnish'];
    }

    # Logrotate
    logrotate::rule { "logrotate_varnishncsa" :
        path          => "/var/log/varnish/varnishncsa.log",
        rotate        => 5,
        rotate_every  => 'day',
        compress      => true,
        delaycompress => true,
        missingok     => true,
        sharedscripts => true,
        postrotate    => 'service varnishncsa reload',
    }
}
