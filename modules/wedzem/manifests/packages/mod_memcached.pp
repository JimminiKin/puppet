class wedzem::packages::mod_memcached {

    class { 'memcached':
  		max_memory => 64,
        listen_ip  => '127.0.0.1'
    }

}
