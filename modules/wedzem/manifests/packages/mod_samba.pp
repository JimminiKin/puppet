class wedzem::packages::mod_samba {

    if $::env == 'dev' or $::env == 'integ' {
        # Mise en place Munin Standard
        class {'samba::server':
            workgroup => 'WORKGROUP',
            server_string => "Vagrant Samba DEV",
            security => 'share'
        }

        samba::server::share {
            'www':
                comment => 'Vagrant Samba DEV',
                path => '/home/admin/www',
                public => true,
                writable => true,
                browsable => true,
                guest_only => true,
                guest_ok => true,
                guest_account => "admin",
                create_mask => 0644,
                force_create_mask => 0644,
                directory_mask => 0775,
                force_directory_mask => 0775,
                force_group => 'www-data',
                force_user => 'admin'
        }
    }
}