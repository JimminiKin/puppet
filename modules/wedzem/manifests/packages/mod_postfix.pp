class wedzem::packages::mod_postfix {

	# Postfix to send mail
    class { 'postfix': }

    # Ensure sendmail not installed (because is already on port 25)
    package { 'sendmail':
        ensure => 'purged'
    }

}