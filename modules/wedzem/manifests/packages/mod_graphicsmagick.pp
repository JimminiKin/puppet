class wedzem::packages::mod_graphicsmagick (
  $gm_base_source = "http://ftp.icm.edu.pl/pub/unix/graphics/GraphicsMagick/1.3",
  $gm_version = "1.3.20",
  $jpeg_base_source = "http://www.ijg.org/files",
  $jpeg_version = "9a"
) {
    # JPEG lib installation
    $jpeg_extraction_dir = "jpeg-${jpeg_version}"
    $jpeg_tar = "jpegsrc.v${jpeg_version}.tar.gz"
    $unless_jpeg = "which jpegtran";

    # Download JPEG lib
    exec { 'download_jpeg':
        unless      => $unless_jpeg,
        command     => "wget ${jpeg_base_source}/${jpeg_tar}",
        timeout     => 0,
        cwd         => "/tmp"
    }

    # Extract JPEG lib archive
    exec { 'extract_jpeg' :
        unless      => $unless_jpeg,
        command     => "tar -xvf ${jpeg_tar}",
        cwd         => "/tmp",
        require    => Exec['download_jpeg']
    }

    # Compile JPEG lib
    exec { 'configure_jpeg' :
        unless      => $unless_jpeg,
        command     => "/bin/sh -c './configure'",
        cwd         => "/tmp/${jpeg_extraction_dir}",
        timeout     => 0,
        require    => Exec['extract_jpeg']
    }

    exec { 'make_jpeg' :
        unless      => $unless_jpeg,
        command     => "make",
        cwd         => "/tmp/${jpeg_extraction_dir}",
        timeout     => 0,
        require    => Exec['configure_jpeg']
    }

    exec { 'make_install_jpeg' :
        unless      => $unless_jpeg,
        command     => "make install",
        cwd         => "/tmp/${jpeg_extraction_dir}",
        timeout     => 0,
        require    => Exec['make_jpeg']
    }

    # Remove sources under /tmp
    exec { 'cleanup_jpeg' :
        onlyif      => "test -f /tmp/${jpeg_tar}",
        command     => "rm -rf /tmp/${jpeg_extraction_dir}; rm -rf /tmp/${jpeg_tar}",
        require    => Exec['make_install_jpeg']
    }

    # GraphicsMagick installation
    $gm_base_file = "GraphicsMagick-${gm_version}"
    $gm_tar = "${gm_base_file}.tar.gz"
    $unless_gm = "which gm"

    # Download GraphicsMagick
    exec { 'download_gm':
        unless      => $unless_gm,
        require      => Exec['make_install_jpeg'],
        command     => "wget ${gm_base_source}/${gm_tar}",
        timeout     => 0,
        cwd         => "/tmp"
    }

    # Extract GM archive
    exec { 'extract_gm' :
        unless      => $unless_gm,
        command     => "tar -xvf ${gm_tar}",
        cwd         => "/tmp",
        require    => Exec['download_gm']
    }

    # Compile GraphicsMagick
    exec { 'configure_gm' :
        unless      => $unless_gm,
        command     => "/bin/sh -c './configure --enable-magick-compat'",
        cwd         => "/tmp/${gm_base_file}",
        timeout     => 0,
        require    => Exec['extract_gm']
    }

    exec { 'make_gm' :
        unless      => $unless_gm,
        command     => "make",
        cwd         => "/tmp/${gm_base_file}",
        timeout     => 0,
        require    => Exec['configure_gm']
    }

    exec { 'make_install_gm' :
        unless      => $unless_gm,
        command     => "make install",
        cwd         => "/tmp/${gm_base_file}",
        timeout     => 0,
        require    => Exec['make_gm']
    }

    # Remove sources under /tmp
    exec { 'cleanup_gm' :
        onlyif      => "test -f /tmp/${gm_tar}",
        command     => "rm -rf /tmp/${gm_base_file}; rm -rf /tmp/${gm_tar}",
        require    => Exec['make_install_gm']
    }
}