class wedzem::packages::mod_nodejs {

    # Node install
    package { 'nodejs':
        ensure => 'present'
    }

    exec {
        'forever':
            command => 'sudo npm install -g forever',
            require => Package["nodejs"],
            timeout => 0,
            unless  => "which forever";

        'forever-service':
            command => 'sudo npm install -g forever-service',
            require => Package["nodejs"],
            timeout => 0,
            unless  => "which forever-service";

        'bower':
            command => 'sudo npm install -g bower',
            require => Package["nodejs"],
            timeout => 0,
            unless  => "which bower";

        'grunt':
            command => 'sudo npm install -g grunt-cli',
            require => Package["nodejs"],
            timeout => 0,
            unless  => "which grunt";

        'gulp':
            command => 'sudo npm install -g gulp',
            require => Package["nodejs"],
            timeout => 0,
            unless  => "which gulp";

        'less':
            command => 'sudo npm install -g less@2.2.0',
            require => Package["nodejs"],
            timeout => 0,
            unless  => "which lessc";
    }

}
