class wedzem::packages::mod_fail2ban {

    # Installation fail2ban
    class { 'fail2ban':
        maxretry => '2',
        mailto => 'admin@wedzem.fr',
        jails => ['ssh', 'postfix'],
    }

    fail2ban::jail { 'ssh':
      port     => 'ssh',
      filter     => 'sshd',
      maxretry => '2',
    }

    fail2ban::jail { 'postfix':
      port     => 'smtp,ssmtp',
      filter   => 'postfix',
      maxretry => '2',
    }

    # On désactive les notifications mails sur les postes de dev
    if $::env == 'dev' {
        fail2ban::action { 'sendmail':
            actionstart => 'echo "Fail2ban start"',
            actionstop => 'echo "Fail2ban stop"',
        }

        fail2ban::action { 'sendmail-whois':
            actionstart => 'echo "Fail2ban start"',
            actionstop => 'echo "Fail2ban stop"',
        }

        fail2ban::action { 'sendmail-whois-lines':
            actionstart => 'echo "Fail2ban start"',
            actionstop => 'echo "Fail2ban stop"',
        }
    }

    Class['fail2ban'] -> munin::plugin { 'fail2ban': linkplugins => true }
}
