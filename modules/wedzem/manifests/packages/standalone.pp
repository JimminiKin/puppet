class wedzem::packages::standalone {

    # Package indispensables
    ensure_packages([
        'curl',
        'wget',
        'tar',
        'acl',
        'htop',
        'unzip',
        'ccze',
        'nano',
        'makepasswd',
        'tree',
        'telnet',
        'vim',
        'locales-all',
        'dnsutils',
        'iperf',
        'iotop',
        'sysstat',
        'build-essential',
        'apt-transport-https',
        'apt-show-versions', # Pour trouver les paquets unstable
        'libsasl2-modules', # Pour Postfix with MandrillApp
        'perl', # Pour les sondes ES
        'libjson-perl', # Pour les sondes ES
        'libxml-perl', # Pour les sondes Varnish
        'libcache-cache-perl', # Pour les sondes Mysql
        'libpng12-0', # Pour effectuer du traitement d'images png
        'libpng12-dev',
        'libxrender1', # Pour wkhtmltopdf
        'screen'
    ])

}
