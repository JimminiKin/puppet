class wedzem::packages::mod_postgresql {

    # Installation Postgresql
    class { 'postgresql::globals':
	  encoding => 'UTF8',
	  version => '9.1',
	}->
    class { 'postgresql::server':
		listen_addresses           => '*',
        postgres_password => 'kVXFkgXhFyHaBKpUX5w4BLFNmJhr',
    }


    file {
        # Create /var/lib/postgresql/9.1/main/archive directory
        '/var/lib/postgresql/9.1/main/archive':
            ensure => 'directory',
            owner => 'postgres',
            group => 'postgres',
            mode  => '0700';
    }



}