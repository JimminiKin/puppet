class wedzem::clusters::monitor::web {

    # login ...... munin
    # password ... ShVugNW5r1b3F
    file { '/etc/munin/htpasswd' :
        owner  => 'root',
        group  => 'root',
        mode  => '0644',
        source => 'puppet:///modules/wedzem/clusters/monitor/htpasswd',
    }

    # Mise en place du vhost de Munin SERVER
    apache::vhost {
        "vhost_munin":
            add_listen      => true,
            setenv          => ["ENV_NAME ${::env}"],
            ip              => '*',
            port            => '80',
            docroot         => "/var/cache/munin/www",
            docroot_owner   => 'munin',
            docroot_group   => 'munin',
            servername      => 'monitor.wedzem.fr',
            serveraliases   => "monitor.wedzem.rc-${::env}.com",
            options         => ['-Indexes','FollowSymLinks','MultiViews'],
            override        => ['All'],
            aliases         => [
                { alias => '/static', path => '/etc/munin/static' },
            ],
            rewrites        => [
                {
                comment       => 'HTML',
                rewrite_cond  => ['%{REQUEST_URI} !^/static', '%{REQUEST_URI} .html$ [or]', '%{REQUEST_URI} =/'],
                rewrite_rule  => ['^/(.*) /usr/lib/munin/cgi/munin-cgi-html/$1 [L]'],
                },
                {
                comment       => 'Images - remove path to munin-cgi-graph, if present',
                rewrite_rule  => ['^/munin-cgi/munin-cgi-graph/(.*) /$1'],
                },
                {
                rewrite_cond  => ['%{REQUEST_URI} !^/static', '%{REQUEST_URI} .png$'],
                rewrite_rule  => ['^/(.*) /usr/lib/munin/cgi/munin-cgi-graph/$1 [L]'],
                }
            ],
            directories         => [
                {
                    path => '/var/cache/munin/www',
                    auth_user_file  => '/etc/munin/htpasswd',
                    auth_name       => "Munin (${::env})",
                    auth_type       => 'Basic',
                    auth_require    => 'valid-user',
                },
            ],
            custom_fragment => '
                # Ensure we can run (fast)cgi scripts
                ScriptAlias /munin-cgi/munin-cgi-graph /usr/lib/munin/cgi/munin-cgi-graph
                <Location /munin-cgi/munin-cgi-graph>
                        Options +ExecCGI
                        <IfModule mod_fcgid.c>
                                SetHandler fcgid-script
                        </IfModule>
                        <IfModule !mod_fcgid.c>
                                SetHandler cgi-script
                        </IfModule>
                        Allow from all
                </Location>

                ScriptAlias /munin-cgi/munin-cgi-html /usr/lib/munin/cgi/munin-cgi-html
                <Location /munin-cgi/munin-cgi-html>
                        Options +ExecCGI
                        <IfModule mod_fcgid.c>
                                SetHandler fcgid-script
                        </IfModule>
                        <IfModule !mod_fcgid.c>
                                SetHandler cgi-script
                        </IfModule>
                        Allow from all
                </Location>

                <Location />
                        Options +ExecCGI
                        <IfModule mod_fcgid.c>
                                SetHandler fcgid-script
                        </IfModule>
                        <IfModule !mod_fcgid.c>
                                SetHandler cgi-script
                        </IfModule>
                        Allow from all
                </Location>

                <Location /static/>
                        SetHandler None
                        Allow from all
                </Location>

                <IfModule mod_expires.c>
                    ExpiresActive On
                    ExpiresDefault M310
                </IfModule>',
            directoryindex  => 'index.html',
            require         => [
                Class['munin'],
                File['/etc/munin/htpasswd']
            ]
    }

    # Set munin bootstrap
    exec { "clone_munstrap":
        user    => "root",
        cwd     => "/etc/munin",
        command => "git clone https://github.com/wedzem/munstrap.git /etc/munin/munstrap && rm -rf templates static && cp -r munstrap/templates . && cp -r munstrap/static .",
        unless  => "test -d /etc/munin/munstrap/.git",
        require => [
            Package['git'],
            Class['munin::server']
        ]
    }
}