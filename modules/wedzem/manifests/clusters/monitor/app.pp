class wedzem::clusters::monitor::app {

    case $::env {
        "integ" : {
            $munin_conf = 'internal.conf'
        }
        default: {
            $munin_conf = 'external.conf'
        }
    }

    file { "/etc/munin/munin-conf.d/${munin_conf}" :
        owner => 'root',
        group => 'munin',
        mode => '0644',
        source => "puppet:///modules/wedzem/clusters/monitor/${munin_conf}",
        require => Class['munin::server']
    }

}