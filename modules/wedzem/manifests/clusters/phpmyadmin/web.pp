class wedzem::clusters::phpmyadmin::web {

    #------------------------------------------
    # PhpMyAdmin

    package { "phpmyadmin":
        ensure => latest,
        require => [
            Class['apache'],
            Class['Php::Cli'],
            Class['Php::Fpm']
        ];
    }


    case $::env {
        "prod" : {
           
                file { "/etc/phpmyadmin/config.inc.php":
                    owner => 'root',
                    group => 'www-data',
                    mode  => '0640',
                    source => "puppet:///modules/wedzem/clusters/phpmyadmin/prod.php",
                    require => Package['phpmyadmin'];
                }

                # Installation Apache
                apache::vhost {
                    "vhost_phpmyadmin":
                        add_listen      => true,
                        setenv          => ["RC_ENV ${::env}"],
                        ip              => '*',
                        port            => '80',
                        docroot         => "/usr/share/phpmyadmin",
                        docroot_owner   => 'root',
                        docroot_group   => 'root',
                        servername      => 'phpmyadmin.rc.prod',
                        serveraliases   => "phpmyadmin.rc.${::env}",
                        options         => ['Indexes','FollowSymLinks','MultiViews'],
                        override        => ['All'];
                }


      }
        "integ" : {

                file { "/etc/phpmyadmin/config.inc.php":
                    owner => 'root',
                    group => 'www-data',
                    mode  => '0640',
                    source => "puppet:///modules/wedzem/clusters/phpmyadmin/integ.php",
                    require => Package['phpmyadmin'];
                }

                # Installation Apache
                apache::vhost {
                    "vhost_phpmyadmin":
                        add_listen      => true,
                        setenv          => ["RC_ENV ${::env}"],
                        ip              => '*',
                        port            => '80',
                        docroot         => "/usr/share/phpmyadmin",
                        docroot_owner   => 'root',
                        docroot_group   => 'root',
                        servername      => 'phpmyadmin.rc.integ',
                        serveraliases   => "phpmyadmin.wedzem.dev",
                        options         => ['Indexes','FollowSymLinks','MultiViews'],
                        override        => ['All'];
                }
                        
        }
    }

}