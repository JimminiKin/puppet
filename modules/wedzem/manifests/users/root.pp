class wedzem::users::root {

    # Mise en place des répertoires 'www', '.ssh' et 'logs'
    file {
        '/root/.bashrc':
            owner => 'root',
            group => 'root',
            mode  => '0640',
            source => 'puppet:///modules/wedzem/users/bashrc_root';

        '/root/.bash_aliases':
            owner => 'root',
            group => 'root',
            mode  => '0640',
            source => 'puppet:///modules/wedzem/users/bash_aliases';

        '/root/.profile':
            owner => 'root',
            group => 'root',
            mode  => '0640',
            source => 'puppet:///modules/wedzem/users/profile';

        # Create .ssh directory
        '/root/.ssh':
            ensure => 'directory',
            owner => 'root',
            group => 'root',
            mode  => '0770';

        '/root/.ssh/id_rsa':
            owner => 'root',
            group => 'root',
            mode  => '0600',
            source => 'puppet:///modules/wedzem/users/ssh/id_rsa',
            require => File['/root/.ssh'];

        '/root/.ssh/id_rsa.pub':
            owner => 'root',
            group => 'root',
            mode  => '0600',
            source => 'puppet:///modules/wedzem/users/ssh/id_rsa.pub',
            require => File['/root/.ssh'];
    }

    # Ajouter les clés dans known_hosts
    $hosts = [
        'git.wedzem.dev',
        'gitlab.wedzem.dev',
        'github.com',
        'bitbucket.org'
    ]

    each($hosts) |$host| {
        exec {
            "root_knownhosts_${host}" :
                command => "ssh-keyscan -H ${host} >> /root/.ssh/known_hosts && echo ${host} >> /root/.ssh/known_hosts.md",
                user => 'root',
                require => File['/root/.ssh'],
                unless => "grep -Fxq '${host}' /root/.ssh/known_hosts.md"
        }
    }
}
