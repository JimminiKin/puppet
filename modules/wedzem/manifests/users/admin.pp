
class wedzem::users::admin {

    # Création du user 'admin' et de son homedir
    user {
      'admin':
        ensure => present,
        gid => 'www-data',
        groups => ['admin', 'www-data'],
        membership => minimum,
        shell => '/bin/bash',
        home => '/home/admin',
        password => 'brEDrAs3qA7este8',
        require => [
            Group['admin'],
            Group['www-data']
        ]
    }

    group {
      'www-data':
        ensure => present,
        gid => '33';
    }

    group {
      'admin':
        ensure => present,
    }

    # Donner les droits sudo à "admin"
    sudoers{ 'admin':
        type => 'user_spec',
        users => 'admin',
        commands => 'NOPASSWD: ALL',
        hosts => 'ALL',
        require => User['admin']
    }

    # Subtilité pour le montage NFS
    if $::env == 'dev' {
        file {
            '/home/admin/tmp':
                ensure => 'directory',
                owner => 'admin',
                group => 'www-data',
                mode  => '0775',
                require => File['/home/admin'];
        }

        exec {
            'setacl_tmp':
                command => "sudo setfacl -R -m u:www-data:rwx -m u:admin:rwx /home/admin/tmp",
                require => [
                    File['/home/admin/tmp'],
                    Package['acl']
                ];

            'setacl_R_tmp':
                command => "sudo setfacl -dR -m u:www-data:rwx -m u:admin:rwx /home/admin/tmp",
                require => Exec['setacl_tmp'];
        }
    }

    file {
        '/home/admin/www':
            ensure => 'directory',
            owner  => $owner::admin,
            group  => $group::wwwdata,
            mode  => '0755',
            require => File['/home/admin'];
    }

    # Mise en place des répertoires 'www', '.ssh' et 'logs'
    file {

        # Create /home/admin directory
        '/home/admin':
            ensure => 'directory',
            owner => 'admin',
            group => 'admin',
            mode  => '0755',
            require => Sudoers['admin'];

        '/home/admin/.bashrc':
            owner => 'admin',
            group => 'admin',
            mode  => '0600',
            source => 'puppet:///modules/wedzem/users/bashrc_admin',
            require => File['/home/admin'];

        '/home/admin/.bash_aliases':
            owner => 'admin',
            group => 'admin',
            mode  => '0600',
            source => 'puppet:///modules/wedzem/users/bash_aliases',
            require => File['/home/admin'];

        '/home/admin/.profile':
            owner => 'admin',
            group => 'admin',
            mode  => '0600',
            source => 'puppet:///modules/wedzem/users/profile',
            require => File['/home/admin'];

        # Create logs directory
        '/home/admin/logs':
            ensure => 'directory',
            owner  => 'admin',
            group  => 'www-data',
            mode  => '0775',
            require => File['/home/admin'];

        # Create templates directory
        '/home/admin/templates':
            ensure => 'directory',
            owner  => 'admin',
            group  => 'admin',
            mode  => '0750',
            source => 'puppet:///modules/wedzem/users/templates',
            recurse => true,
            require => File['/home/admin'];

        # Create .ssh directory
        '/home/admin/.ssh':
            ensure => 'directory',
            owner  => 'admin',
            group  => 'admin',
            mode  => '0750',
            require => File['/home/admin'];

        '/home/admin/.ssh/authorized_keys':
            owner => 'admin',
            group => 'admin',
            mode  => '0600',
            source => "puppet:///modules/wedzem/users/ssh/authorized_keys/${::env}",
            require => File['/home/admin/.ssh'];

        '/home/admin/.ssh/id_rsa':
            owner => 'admin',
            group => 'admin',
            mode  => '0600',
            source => 'puppet:///modules/wedzem/users/ssh/id_rsa',
            require => File['/home/admin/.ssh'];

        '/home/admin/.ssh/id_rsa.pub':
            owner => 'admin',
            group => 'admin',
            mode  => '0600',
            source => 'puppet:///modules/wedzem/users/ssh/id_rsa.pub',
            require => File['/home/admin/.ssh'];
    }

    # Ajouter les clés dans known_hosts
    $hosts = [
        'git.wedzem.dev',
        'gitlab.wedzem.dev',
        'github.com',
        'bitbucket.org'
    ]

    each($hosts) |$host| {
        exec {
            "admin_knownhosts_${host}" :
                command => "ssh-keyscan -H ${host} >> /home/admin/.ssh/known_hosts && echo ${host} >> /home/admin/.ssh/known_hosts.md",
                user => 'admin',
                require => File['/home/admin/.ssh'],
                unless => "grep -Fxq '${host}' /home/admin/.ssh/known_hosts.md"
        }
    }
}
