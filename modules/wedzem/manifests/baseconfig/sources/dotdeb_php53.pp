class wedzem::baseconfig::sources::dotdeb_php53 inherits wedzem::baseconfig::sources {

    apt::source { 'dotdeb_squeeze':
        location => "http://packages.dotdeb.org",
        release => "squeeze",
        repos => "all",
        key => "89DF5277",
        key_server => "keys.gnupg.net",
        include_src => false
    }

    apt::source { 'debian_squeeze':
        location => "http://ftp.fr.debian.org/debian",
        release => "squeeze",
        repos => "main contrib non-free",
        include_src => false
    }

    apt::pin { 'dotdeb_squeeze_php':
        packages => '*php*',
        priority => 1000,
        codename => 'squeeze' # n=
    }

}
