class wedzem::baseconfig::sources::jenkins inherits wedzem::baseconfig::sources {

    apt::source { 'jenkins':
        location => 'http://pkg.jenkins-ci.org/debian-stable',
        release => 'binary/',
        repos => '',
        key => 'D50582E6',
        key_source => 'http://pkg.jenkins-ci.org/debian/jenkins-ci.org.key',
        include_src => false,
        pin => 1000
    }

}
