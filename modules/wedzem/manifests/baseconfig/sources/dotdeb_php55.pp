class wedzem::baseconfig::sources::dotdeb_php55 inherits wedzem::baseconfig::sources {

    apt::source { 'dotdeb':
        location => "http://packages.dotdeb.org",
        release => "wheezy-php55",
        repos => "all",
        key => "89DF5277",
        key_server => "keys.gnupg.net",
        include_src => false,
        pin => 1000
    }

}
