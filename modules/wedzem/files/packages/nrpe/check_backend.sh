##################################################################
# Creation: Julien Charge
# Last Modification: 6 Juin 2014
# This script is polling if testd daemon is running
##################################################################
STATE_OK=0
STATE_WARNING=1
STATE_CRITICAL=2
STATE_UNKNOWN=3
STATE_DEPENDENT=4
ps auxw | grep [t]estd | grep -v nagios > /dev/null
STATE=0

NB_SERVEUR_DOWN=`varnishadm backend.list | grep Sick | wc -l`;

if [ "$NB_SERVEUR_DOWN" = "0" ]
then
echo "Backends OK"
exit 0
else
echo " $NB_SERVEUR_DOWN backend(s) DOWN"
exit $STATE_CRITICAL
fi