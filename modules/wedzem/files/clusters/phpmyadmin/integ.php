<?php

/**
 * This example configuration shows how to configure phpMyAdmin for
 * many hosts that all have identical configuration otherwise. To add
 * a new host, just drop it into $hosts below. Contributed by
 * Matthew Hawkins.
 */

// Load secret generated on postinst
include '/var/lib/phpmyadmin/blowfish_secret.inc.php';

$i = 0;
$hosts = array (
    "Drupal (integ)" => "drupal.rc.integ",
    "API (integ)" => "api.rc.integ",
    "API (mathieu)" => "mathieu.rc.integ",
    "Supervision (integ)" => "supervision.rc.integ",
    "Tourism System (integ)" => "ts.rc.integ",
    "Orocrm (Amine)" => "amine.orocrm.rc.dev",
);

foreach ($hosts as $name => $host) {
    $i++;
    $cfg['Servers'][$i]['verbose'] = $name;
    $cfg['Servers'][$i]['host'] = $host;
    $cfg['Servers'][$i]['connect_type'] = 'tcp';
    $cfg['Servers'][$i]['extension'] = 'mysql';
    $cfg['Servers'][$i]['compress'] = FALSE;
    $cfg['Servers'][$i]['AllowNoPassword'] = FALSE;
}