<?php

/**
 * This example configuration shows how to configure phpMyAdmin for
 * many hosts that all have identical configuration otherwise. To add
 * a new host, just drop it into $hosts below. Contributed by
 * Matthew Hawkins.
 */

// Load secret generated on postinst
include '/var/lib/phpmyadmin/blowfish_secret.inc.php';

$i = 0;
$hosts = array (
    "Drupal (preprod)" => "drupal.rc.preprod",
    "Drupal (prod)" => "drupaldb1.rc.prod",
    "Drupal Autre (preprod)" => "dau.rc.preprod",
    "Drupal Autre 1 (prod)" => "dausql1.rc.prod",
    "Drupal Autre 2 (prod)" => "dausql2.rc.prod",
    "API (preprod)" => "api.rc.preprod",
    "API (prod)" => "apidb1.rc.prod",
    "Supervision (preprod)" => "supervision.rc.preprod",
    "Supervision (prod)" => "supervision.rc.prod",
    "Tourism System (preprod)" => "ts.rc.preprod",
    "Tourism System (prod)" => "tsdb1.rc.prod",
    "Baikal (prod)" => "baikal.rc.prod",
    "Imports (prod)" => "imports.rc.prod",
    "Mobitour (prod)" => "mobitour.dnsroute.fr",
    "Libswedzem (prod)" => "libswedzem.dnsroute.fr",
    "ftpwedzemfr (prod)" => "ftp.wedzem.fr",
    "immo system (prod)" => "178.33.169.160",
    "LibDrupal (prod)" => "libdrupal.dnsroute.fr",
    "TS MySQL (prod)" => "37.59.230.139",
    "TS Serveur (prod)" => "178.33.169.145",
    "Push Mobile (prod)" => "178.33.169.167",
    "Astuss (prod)" => "astuss.dnsroute.fr",
    "10 14 0 37 (prod)" => "10.14.0.37",
    "10 17 0 2 (prod)" => "10.17.0.2",
    "Manager (prod)" => "manager.dnsroute.fr",
);

foreach ($hosts as $name => $host) {
    $i++;
    $cfg['Servers'][$i]['verbose'] = $name;
    $cfg['Servers'][$i]['host'] = $host;
    $cfg['Servers'][$i]['connect_type'] = 'tcp';
    $cfg['Servers'][$i]['extension'] = 'mysql';
    $cfg['Servers'][$i]['compress'] = FALSE;
    $cfg['Servers'][$i]['AllowNoPassword'] = FALSE;
}