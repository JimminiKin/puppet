<?php

/**
 * This example configuration shows how to configure phpMyAdmin for
 * many hosts that all have identical configuration otherwise. To add
 * a new host, just drop it into $hosts below. Contributed by
 * Matthew Hawkins.
 */

// Load secret generated on postinst
include '/var/lib/phpmyadmin/blowfish_secret.inc.php';

$i = 0;
$hosts = array (
    "Drupal (integ)" => "drupal.rc.integ",
    "Drupal (preprod)" => "drupal.rc.preprod",
    "Drupal (prod)" => "drupaldb1.rc.prod",
    "Drupal Autre (preprod)" => "dau.rc.preprod",
    "Drupal Autre (prod)" => "dausql1.rc.prod",
    "API (integ)" => "api.rc.integ",
    "API (preprod)" => "api.rc.preprod",
    "API (prod)" => "apidb1.rc.prod",
    "API (mathieu)" => "mathieu.rc.integ",
    "Supervision (integ)" => "supervision.rc.integ",
    "Supervision (preprod)" => "supervision.rc.preprod",
    "Supervision (prod)" => "supervisiondb1.rc.prod",
    "Tourism System (integ)" => "ts.rc.integ",
    "Tourism System (preprod)" => "ts.rc.preprod",
    "Tourism System (prod)" => "tsdb1.rc.prod",
    "Baikal (prod)" => "baikal.rc.prod",
    "Imports (prod)" => "imports.rc.prod",
);

foreach ($hosts as $name => $host) {
    $i++;
    $cfg['Servers'][$i]['verbose'] = $name;
    $cfg['Servers'][$i]['host'] = $host;
    $cfg['Servers'][$i]['connect_type'] = 'tcp';
    $cfg['Servers'][$i]['extension'] = 'mysql';
    $cfg['Servers'][$i]['compress'] = FALSE;
    $cfg['Servers'][$i]['AllowNoPassword'] = FALSE;
}
