# wedzem PUPPET

## Lancer une mise mise à jour

```
cd /home/admin/puppet
git pull
git submodule update --init --recursive
sudo FACTER_env='dev' FACTER_cluster='forge' FACTER_roles='web,app,db' FACTER_hieradata_dir='/home/admin/puppet/current/hieradata' puppet apply /home/admin/puppet/current/manifests/default.pp --modulepath=/home/admin/puppet/current/modules --hiera_config /home/admin/puppet/current/hiera.yaml --parser future
```
